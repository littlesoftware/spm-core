#ifndef SPM_VARSERIALIZER_H
#define SPM_VARSERIALIZER_H

#include "Variant.h"
#include <iostream>

// service processing model
namespace spm {

class Serializer
{
public:
    // serializer to/from binary data format
    static bool dumpToBinary(const Variant& object, Variant::Data& data);
    static bool loadFromBinary(const Variant::Data& data, Variant& object);

    // serializer to/from plain text format
    static bool dumpToPlainText(const Variant& object, std::string& text);
    static bool loadFromPlainText(const std::string& str, Variant& object);

private:
    // helper funtions for binary data format
    static Variant::Data sizeToDataBlock(size_t size);
    static size_t dataBlockToSize(const void* data, int &bytes);
    static size_t loadFromBinary(const void* data, size_t size, Variant &object);

    // helpter funtions for plain text format
    static bool dumpPlainTextToStream(const Variant& object, std::ostream& output, const std::string& space);
    static bool loadPlainTextFromStream(std::istream& stream, Variant& object);

};

} // namespace spm

#endif // SPM_VARSERIALIZER_H
