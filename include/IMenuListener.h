#ifndef SPM_IMENULISTENER_H
#define SPM_IMENULISTENER_H

#include "Menu.h"
#include <inttypes.h>

// service processing model
namespace spm {

class IMenuListener
{
public:
    virtual ~IMenuListener() = default;

    // open menu
    virtual void onOpenMenu(const Menu* menu) = 0;
    // close menu
    virtual void onCloseMenu() = 0;
    // action menu
    virtual void onMenuAction(uint32_t action) = 0;
};

} // namespace spm

#endif // SPM_IMENULISTENER_H
