#ifndef SPM_TUNING_H
#define SPM_TUNING_H

#include "Parameter.h"
#include <memory>
#include <list>
#include <map>
#include <string>
#include <iostream>

namespace spm {

class Tuning
{
    friend class Parameter;
public:
    Tuning();
    Parameter& makeFlag();
    Parameter& makeParameter();
    Parameter& makeOperand();

    // parse stream
    void parse(std::istream &stream);
    // parse config file
    void parse(const std::string& filename);
    // parse argument of command line
    void parse(int argc, const char* argv[]);
    // get parameter by name
    const Parameter& getParameter(const std::string& name) const;
    const Parameter& getParameter(char name) const;
    // get flag by name
    const bool hasFlag(const std::string& name) const;
    const bool hasFlag(const char name) const;
    // get operand by name
    const Parameter& getOperand(const std::string& name) const;
    const Parameter& getOperand(char name) const;
    // print help of commands
    void printHelp(const std::string& description, std::ostream& output = std::cout) const;
    // get program name
    const std::string& getProgramName() const;
private:
    // types
    typedef std::shared_ptr<Parameter> ParameterPtr;
    typedef std::map<Parameter*,ParameterPtr> ParameterRefList;
    typedef std::map<std::string,ParameterPtr> NamedMap;
    typedef std::list<std::string> StringList;
    // methods
    void setNameOption(const std::string& name, Parameter* parameter);
    void setNameOperand(const std::string& name, Parameter* parameter);
    std::string trimComment(const std::string& str) const;
    std::string trimSpace(const std::string& str) const;
    std::string parseProgramName(const char* name) const;
    inline ParameterPtr getParameterByName(const std::string& name) const;
    inline ParameterPtr getParameterByName(char name) const;
    inline std::logic_error generateLogicError(const char* fmt, ...) const;
    inline std::logic_error generateLogicError(const char* fmt, ...);
    // memory data
    ParameterRefList  m_parameters;          // list all parameters
    NamedMap          m_namedParameters;     // list of named parameters
    StringList        m_operands;            // list of operands
    NamedMap          m_namedOperands;       // list of named operands
    std::string       m_programName;
    bool              m_settedMode;          // mode of setted value
};

} // namespace spm

#endif // SPM_TUNING_H
