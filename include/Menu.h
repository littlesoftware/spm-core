#ifndef SPM_MENU_H
#define SPM_MENU_H

#include <memory>
#include <vector>
#include <initializer_list>

// service processing model
namespace spm {

// menu object
class Menu
{
public:
    // types
    typedef std::vector<Menu> MenuList;
    typedef MenuList::const_iterator MenuListIterator;
    typedef std::vector<uint32_t> ActionList;

    explicit Menu();
    explicit Menu(const std::string& name);
    explicit Menu(const std::string& name, const std::initializer_list<Menu>& menuList);
    ~Menu();

    Menu& addSubmenu(const Menu& menu);

    Menu& setName(const std::string& name);
    const std::string& getName() const;

    Menu& addAction(uint32_t action);

    Menu& setVariable();
    bool isVariable() const;

    const ActionList& getActions() const;

    bool isEmpty() const;
    int getSize() const;
    MenuListIterator begin() const;
    MenuListIterator end() const;
    Menu* getSubmenuByIndex(int index);

    void setIndex(int index);
    Menu& setValue(const std::string& value);
    const std::string& getValue() const;

private:
    //types
    // methods
    // memory data

    MenuList m_menuList;
    std::string m_name;
    int m_menuListCounter   {0};
    bool m_isVariable       {false};
    std::string m_value;                // value if m_menuList.empty();
    int m_index             {-1};       // selected m_menuList item
    ActionList m_actions;
};

} // namespace spm

#endif // SPM_MENU_H
