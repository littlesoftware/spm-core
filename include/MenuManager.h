#ifndef SPM_MENUMANAGER_H
#define SPM_MENUMANAGER_H

#include "IMenuListener.h"
#include "Menu.h"
#include <map>
#include <stack>

// service processing model
namespace spm {

// menu manager
class MenuManager
{
public:
    // types
    enum Action: uint32_t
    {
        NONE = 0,
        CLOSE_MENU = 1,
        OTHER = 1000        // other action since 1000+
    };

    MenuManager(IMenuListener* listener);

    // add some menu
    void addMenu(int type, const Menu& menu);
    // show menu
    void showMenu(int type);
    // selected item of menu
    void selectItemMenu(int index);
    // cancel and closr menu
    void cancelMenu();

private:
    typedef std::map<int,Menu> MenuDict;
    typedef std::stack<Menu*> MenuStack;

    MenuDict m_menus;
    MenuStack m_stack;
    IMenuListener* m_listener;
};

} // namespace spm

#endif // SPM_MENUMANAGER_H
