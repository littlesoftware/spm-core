#ifndef SPM_BASESERVICE_H
#define SPM_BASESERVICE_H

#include "IService.h"
#include <string>
#include <list>

namespace spm {

class BaseService: public IService
{
public:
    BaseService(const std::string& serviceName);

    virtual const std::string& getServiceName() const override;
    virtual void getSupportedTypes(TypesCollection& list) const override;
    virtual bool request(const std::string&, const Variant&, Variant&) override;
    virtual void action(const std::string&, const Variant&) override;
    virtual void initialize() override;
    virtual bool tick() override;

    // add new supported type
    void addSupportedType(const std::string& type);

private:
    std::string m_serviceName;
    TypesCollection m_supportedTypes;
};

} // namespace spm

#endif // SPM_BASESERVICE_H
