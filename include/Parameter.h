#ifndef SPM_PARAMETER_H
#define SPM_PARAMETER_H

#include <list>
#include <string>
#include <regex>

namespace spm {

class Tuning;

class Parameter
{
public:
    typedef std::list<std::string> ValueList;

    // constructor of parameter and flag
    Parameter(Tuning* manager, bool isFlag);
    // constructor of argument of command line
    explicit Parameter(Tuning* manager);
    // set name
    Parameter& setName(char name);
    Parameter& setName(const std::string& name);
    // set describe of parameter
    Parameter& setDescription(const std::string& description);
    // set value of parameter
    Parameter& setValue(const std::string& value, int& isOk);
    Parameter& setValue(const std::string& value);
    // return true if parameter is a flag
    bool isFlag() const;
    // get value of parameter (fisrt item of the list)
    const std::string& asString() const;
    bool asBoolean() const;
    int asInteger() const;
    // set multi value (no singleton)
    Parameter& setMultiValues(bool singleton=false);
    // get values list
    const ValueList& getValueList() const;
    // get description
    const std::string& getDescription() const;
    // is singleton
    bool isSingleton() const;
    // set regex checker of value
    Parameter& setCheckerValue(const std::string& regex);
private:
    Tuning* m_manager;      // a pointer on the manager
    bool    m_isFlag;       // is flag
    bool    m_isOperand;    // is operand
    bool    m_isSingleton;  // is singletone (otherwise can store a list of values)
    std::string m_value;    // value of singleton
    bool    m_settedValue;
    ValueList m_values;     // values of multi values
    std::string m_description;  // description of parameter
    std::string m_descValue;    // description of value
    bool    m_useRegEx;     // use regex to check parameter value
    std::regex m_regexChecker;
};

} // namespase spm

#endif // SPM_PARAMETER_H
