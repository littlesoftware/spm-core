#ifndef SPM_SERVICEMANAGER_H
#define SPM_SERVICEMANAGER_H

#include "IService.h"
#include "Variant.h"
#include <string>
#include <map>
#include <memory>
#include <set>

namespace spm {

class ServiceManager
{
public:
    ServiceManager();
    ~ServiceManager();

    // add new service
    void addService(IService* service);
    // return true if service named "name" exist
    bool hasService(const std::string& service) const;
    // send request to some service
    bool performRequest(const std::string& service, const std::string& type, const Variant& args, Variant& result) const;
    // send action
    void performAction(const std::string& type, const Variant& args) const;
    // loop of services
    bool run();

    // get implementation from some service
    template<typename T>
    T* getImplementation(const std::string& service)
    {
        if(hasService(service))
            return dynamic_cast<T*>(m_services.at(service).get());
        return nullptr;
    }

private:
    // ----------------------------------
    // METHODS
    // ----------------------------------
    // initialize services
    inline void initialize();

    // ----------------------------------
    // MEMBER DATA
    // ----------------------------------
    typedef std::shared_ptr<IService> IServicePtr;
    typedef std::map<std::string,IServicePtr> ServiceCollection;
    typedef std::multimap<std::string,IServicePtr> SupportedTypesCollection;
    typedef std::set<IServicePtr> ServiceTicks;
    ServiceCollection m_services;
    SupportedTypesCollection m_supportedTypes;
    ServiceTicks m_activeTicks;
};

} // namespace spm

#endif // SPM_SERVICEMANAGER_H
