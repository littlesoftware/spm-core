#ifndef SPM_VARIANT_H
#define SPM_VARIANT_H

#include <string>
#include <list>
#include <vector>
#include <map>
#include <cstdint>

// service processing model
namespace spm {

// Variant object
class Variant
{
public:
    // types
    enum Type {
        UNKNOWN,
        INTEGER,
        STRING,
        REAL,
        BOOLEAN,
        LIST,
        ARRAY,
        DICT,
        DATA
    };
    typedef int64_t Integer;
    typedef bool Boolean;
    typedef std::string String;
    typedef double Real;
    typedef std::list<Variant> List;
    typedef std::vector<Variant> Array;
    typedef std::pair<std::string,Variant> DictPair;
    typedef std::map<std::string,Variant> Dict;
    typedef std::vector<uint8_t> Data;

    // constructors
    Variant();
    Variant(int value);
    Variant(unsigned int value);
    Variant(Integer value);
    Variant(Boolean value);
    Variant(const char* value);
    Variant(const String& value);
    Variant(Real value);
    Variant(const List& value);
    Variant(const Array& value);
    Variant(const Dict& value);
    Variant(const Data& value);
    Variant(const void* data, size_t size);
    Variant(const std::initializer_list<DictPair>& list);
    Variant(const Variant& obj);
    Variant(Variant &&obj);
    ~Variant();

    // get type
    Type getType() const;

    // get const value
    Integer       toInteger() const;
    Boolean       toBoolean() const;
    const String& toString() const;
    Real          toReal() const;
    const List&   toList() const;
    const Array&  toArray() const;
    const Dict&   toDict() const;
    const Data&   toData() const;

    // cast
    Integer castToInteger() const;
    Boolean castToBoolean() const;
    String  castToString() const;
    Real    castToReal() const;
    List    castToList() const;
    Array   castToArray() const;
    Dict    castToDict() const;
    Data    castToData() const;

    // set
    void setUnknown();
    void setInteger(Integer value);
    void setBoolean(Boolean value);
    void setString(const String& value);
    void setReal(Real value);
    void setList(const List& value);
    void setArray(const Array& value);
    void setDict(const Dict& value);
    void setData(const Data& value);
    void setObject(const Variant& obj);

    // get value for edit
    Integer& getInteger();
    Boolean& getBoolean();
    String&  getString();
    Real&    getReal();
    List&    getList();
    Array&   getArray();
    Dict&    getDict();
    Data&    getData();

    // operators =
    Variant& operator=(int value);
    Variant& operator=(Integer value);
    Variant& operator=(Boolean value);
    Variant& operator=(const char* value);
    Variant& operator=(const String& value);
    Variant& operator=(Real value);
    Variant& operator=(const List& value);
    Variant& operator=(const Array& value);
    Variant& operator=(const Dict& value);
    Variant& operator=(const Data& value);
    Variant& operator=(const Variant& obj);
    Variant& operator=(Variant&& obj);

    // operators
    operator bool() const;
    operator int() const;
    operator double() const;
    operator float() const;

    // compare objects
    bool operator==(const Variant& obj) const;

private:
    inline void stringToInteger(const String& from, Integer& to) const;
    inline void integerToString(Integer from, String& to) const;
    inline void stringToReal(const String& from, Real& to) const;
    inline void realToString(Real from, String& to) const;
    inline void listToString(const List& from, String& to) const;
    inline void arrayToString(const Array& from, String& to) const;
    inline void dictToString(const Dict& from, String& to) const;
    inline void dataToString(const Data& from, String& to) const;

    inline void setUnknownValue();
    inline void setIntegerValue(Integer value);
    inline void setBooleanValue(Boolean value);
    inline void setStringValue(const String& value);
    inline void setRealValue(Real value);
    inline void setListValue(const List& value);
    inline void setArrayValue(const Array& value);
    inline void setDictValue(const Dict& value);
    inline void setDataValue(const Data& value);
    inline void setObjectValue(const Variant& obj);
    inline void moveObjectValue(Variant&& obj);

    inline void freeUpResources();

    Type m_type;

    // the storage of data
    union {
        Integer m_dataInteger;
        Boolean m_dataBoolean;
        Real    m_dataReal;
        String* m_dataString;
        List*   m_dataList;
        Array*  m_dataArray;
        Dict*   m_dataDict;
        Data*   m_dataData;
    };
};

} // namespace spm

#endif // SPM_VARIANT_H
