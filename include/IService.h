#ifndef SPM_ISERVICE_H
#define SPM_ISERVICE_H

#include "Variant.h"
#include <string>
#include <set>

namespace spm {

typedef std::set<std::string> TypesCollection;

class IService
{
public:
    virtual ~IService() = default;

    // get service name
    virtual const std::string& getServiceName() const = 0;
    // get support types
    virtual void getSupportedTypes(TypesCollection& list) const = 0;
    // handle request (return true is okay)
    virtual bool request(const std::string& type, const Variant& args, Variant& result) = 0;
    // handle action (like request, but has no result)
    virtual void action(const std::string& type, const Variant& args) = 0;
    // initialize method
    virtual void initialize() = 0;
    // handle tick of loop
    virtual bool tick() = 0;
};

} // namespace spm

#endif // SPM_ISERVICE_H
