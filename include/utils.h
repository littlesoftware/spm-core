#ifndef SPM_UTILS_H
#define SPM_UTILS_H

#include <string>
#include <stdexcept>

namespace spm {

std::string stringf(const char* fmt, ...);
std::logic_error exceptf(const char* fmt, ...);
std::string dumpHex(const void* data, size_t size);
size_t stringLengthByUtf8(const std::string& text);
std::wstring wcast(const std::string& text);

}

#endif // SPM_UTILS_H
