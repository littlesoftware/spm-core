#ifndef SPM_LOGGER_H
#define SPM_LOGGER_H

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <mutex>

// service processing model
namespace spm {

namespace LoggerFlag {
enum {
    DATE = 0x01,
    TIME = 0x02,
    DATETIME = DATE | TIME,
    NAME = 0x04,
    THREAD = 0x08,
    TYPE = 0x10,
    MILLISECONS = 0x20,
    COLOR = 0x40,
    SEPARATION = DATETIME | NAME | THREAD | TYPE,
    DEFAULT = TIME | TYPE | NAME
};
}

/*
 * Logger
 * Event logging type
 *
 */

class Logger
{
public:
    // types
    enum class Type
    {
        TRACE=0,
        SEGMENT=1,
        DEBUG=2,
        INFO=3,
        WARNING=4,
        ERROR=5,
        CRITICAL=6
    };

    explicit Logger(const std::string& name);
    ~Logger();

    template<typename T>
    void trace(const T& message) {
        translateToLog(message, Type::TRACE);
    }

    template<typename T>
    void debug(const T& message) {
        translateToLog(message, Type::DEBUG);
    }

    template<typename T>
    void info(const T& message) {
        translateToLog(message, Type::INFO);
    }

    template<typename T>
    void warning(const T& message) {
        translateToLog(message, Type::WARNING);
    }

    template<typename T>
    void error(const T& message) {
        translateToLog(message, Type::ERROR);
    }

    template<typename T>
    void critical(const T& message) {
        translateToLog(message, Type::CRITICAL);
    }

    template<typename T>
    Logger& operator<< (const T& message) {
        translateToLog(message, Type::DEBUG);
        return *this;
    }

    template<typename T>
    static void sysTrace(const T& message) {
        system().translateToLog(message, Type::TRACE);
    }

    // Settings
    // low type
    static void setLowType(Type type);

    // input / output stream
    static void setOutput(std::ostream& output);
    static void setOutput(std::ostream& output, std::ostream& error);
    static void setOutputError(std::ostream& error);

    // settings of information to output
    static void addFlag(uint32_t flag);
    static void removeFlag(uint32_t flag);
    static void setFlags(uint32_t flags);

    // redirect to file log output/error
    static bool redirectToFile(const std::string& filename, bool andError=true, bool append=true);      // output and error
    static bool redirectErrorToFile(const std::string& filename, bool append=true); // only error

private:
    struct Settings {
        Type lowType {Type::DEBUG};
        uint32_t flags {LoggerFlag::DEFAULT};
        std::ofstream outputFile, errorFile;
        std::ostream* output {&std::cout};
        std::ostream* error  {&std::cerr};
    };

    Logger();
    static Logger& system();

    template<typename T>
    inline void translateToLog(const T& message, Type type) {
        std::ostringstream buffer;
        buffer << message;
        makeLog(buffer.str(), type);
    }

    void makeLog(const std::string& message, const Type type);
    inline void beginSegment();
    inline void endSegment();
    inline const char *stringOfLevel(Type type);
    inline void printColorOfType(std::ostream &os, Type type);
    inline void printResetColor(std::ostream &os);
    inline void printDateTime(std::ostream &os);
    inline void printThreadId(std::ostream &os);

    // variables
    static std::mutex _mutex;
    static Settings m_settings;
    bool _segment{true};
    std::string _name;
};

} // namespace spm

#endif // SPM_LOGGER_H
