# spm-core

## Table of Contents
- [About](#about)
- [Quick Start](#quick-start)
- [Build shared library](#build-shared-library)
- [Build tests](#build-tests)

## About
SPM (service processing model) core is library of common service modules. The library contains the following modules:
* Configure - intended for project configuration. The configure is able to set default values, read values from a configuration file (like INI file) or take them from command line parameters.
* Variant - implementation of a data structure for the interaction of services. The structure is implemented taking into account the transparency of interaction, so services will not know if they are interacting with a local service or a remote service.
* Serializer - a module for transforming the Variant structure into any other protocol.
* ServiceManager - a module for managing the interaction between services.

## Quick start
1. Introduction:

   Installation requires Git and triggers `git clone https://github.com/littlesoftware/spm-core.git`.
2. Build
   
   Build requires CMake and perform:
   ```bash
   cd spm-core
   mkdir build
   cd build
   cmake ..
   cmake --build .
   ```

## Build shared library

   ```bash
   cd spm-core
   mkdir build
   cd build
   cmake -DBUILD_SHARED=ON ..
   cmake --build .
   ```
   
## Build tests

   Build requires `Google tests` library and perform:
   ```bash
   cd spm-core
   mkdir build
   cd build
   cmake -DBUILD_TESTS=ON ..
   cmake --build .
   ctest
   ```
   
