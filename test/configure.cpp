#include "Tuning.h"
#include <gtest/gtest.h>
#include <sstream>
#include <tuple>

TEST(configure, defParameters)
{
    // configure
    spm::Tuning conf;
    conf.makeParameter().setName("address").setValue("127.0.0.1");
    conf.makeParameter().setName("user").setName('u').setValue("user");
    conf.makeFlag().setName("help").setName('H');
    // testing
    ASSERT_EQ( conf.getParameter("address").asString(), "127.0.0.1" );
    ASSERT_EQ( conf.getParameter('u').asString(), "user" );
    ASSERT_EQ( conf.hasFlag("help"), false );
    ASSERT_EQ( conf.hasFlag('H'), false );
}

TEST(configure, stream)
{
    // configure
    spm::Tuning conf;
    conf.makeParameter().setName("address").setValue("127.0.0.1");
    conf.makeParameter().setName("user").setName('u').setValue("user");
    conf.makeParameter().setName('P').setValue("password");
    conf.makeFlag().setName("help").setName('H');
    conf.makeFlag().setName("ask").setName('a');
    conf.makeFlag().setName('Q').setName("q").setValue("yes");
    // testing
    ASSERT_EQ( conf.getParameter("address").asString(), "127.0.0.1" );
    ASSERT_EQ( conf.getParameter("user").asString(), "user" );
    ASSERT_EQ( conf.getParameter('u').asString(), "user" );
    ASSERT_EQ( conf.getParameter('P').asString(), "password" );
    ASSERT_EQ( conf.hasFlag('H'), false );
    ASSERT_EQ( conf.hasFlag("ask"), false );
    ASSERT_EQ( conf.hasFlag('Q'), true );
    // load from stream
    std::stringstream ss;
    ss << "; some config file"     << std::endl
       << "[main] ; main section"  << std::endl
       << "address=12345"          << std::endl // ignore
       << "a=yes        "          << std::endl // ignore
       << "ask=true     "          << std::endl // ignore
       << ";[parameters]"          << std::endl // ignore
       << "a=yes"                  << std::endl // ignore
       << " [  tuning  ]   "       << std::endl // section of parameters
       << " help=Yes "             << std::endl // enable help flag
       << "Q=no  ; disable flag Q" << std::endl // disable flag Q
       << "       ; some comment " << std::endl // ignore
       << "crazzy string!!!"       << std::endl // ignore
       << "P=qwerty; password!"    << std::endl // set parameter
       << " address=  localhost\t" << std::endl // set parameter
       << " [ other ] "            << std::endl // start other section
       << "user=admin"             << std::endl // ignore
       << "u = administrator"      << std::endl; // igore
    conf.parse(ss);
    // testing
    ASSERT_EQ( conf.getParameter("address").asString(), "localhost" );
    ASSERT_EQ( conf.getParameter("user").asString(), "user" );
    ASSERT_EQ( conf.getParameter('u').asString(), "user" );
    ASSERT_EQ( conf.getParameter('P').asString(), "qwerty" );
    ASSERT_EQ( conf.hasFlag('H'), true );
    ASSERT_EQ( conf.hasFlag("ask"), false );
    ASSERT_EQ( conf.hasFlag('Q'), false );
    ASSERT_EQ( conf.hasFlag('q'), false );
    ASSERT_EQ( conf.hasFlag("q"), false );
}

TEST(configure, arguments)
{
    // configure
    spm::Tuning conf;
    conf.makeParameter().setName("address").setValue("127.0.0.1");
    conf.makeParameter().setName('u').setValue("user");
    conf.makeFlag().setName("help").setName('H');
    conf.makeFlag().setName("ask").setValue("yes");
    // testing A
    const char* argsA[] = {"./test",};
    ASSERT_NO_THROW( conf.parse(1, argsA) );
    ASSERT_EQ( conf.getParameter("address").asString(), "127.0.0.1" );
    ASSERT_EQ( conf.getParameter("u").asString(), "user" );
    ASSERT_EQ( conf.hasFlag("help"), false );
    ASSERT_EQ( conf.hasFlag('H'), false );
    ASSERT_EQ( conf.hasFlag("ask"), true );
    // testing B
    const char* argsB[] = {"./test", "-u", "admin", "--address", "mail.com", "--ask=no"};
    ASSERT_NO_THROW( conf.parse(6, argsB) );
    ASSERT_EQ( conf.getParameter("address").asString(), "mail.com" );
    ASSERT_EQ( conf.getParameter("u").asString(), "admin" );
    ASSERT_EQ( conf.hasFlag("help"), false );
    ASSERT_EQ( conf.hasFlag('H'), false );
    ASSERT_EQ( conf.hasFlag("ask"), false );
}

TEST(configure, simpleOperands)
{
    // configure
    spm::Tuning conf;
    conf.makeParameter().setName("address").setValue("127.0.0.1");
    conf.makeParameter().setName('u').setValue("user");
    conf.makeFlag().setName("help").setName('H');
    conf.makeOperand().setName("protocol");
    conf.makeOperand().setName("port");
    // testing
    const char* args[] = {"./test", "-uAdmin", "--address=localhost", "tcp/ip", "8080"};
    ASSERT_NO_THROW( conf.parse(5, args) );
    ASSERT_EQ( conf.getParameter("address").asString(), "localhost" );
    ASSERT_EQ( conf.getParameter("u").asString(), "Admin" );
    ASSERT_EQ( conf.hasFlag("help"), false );
    ASSERT_EQ( conf.getOperand("protocol").asString(), "tcp/ip" );
    ASSERT_EQ( conf.getOperand("port").asString(), "8080" );
}

TEST(configure, singletonParameter)
{
    // configure
    spm::Tuning conf;
    conf.makeParameter().setName("host").setValue("localhost");
    // testing
    const char* args[] = {"./test", "--host=google.com"};
    ASSERT_NO_THROW( conf.parse(2, args) );
    ASSERT_EQ( conf.getParameter("host").asString(), "google.com" );
    // configure
    spm::Tuning conf2;
    conf2.makeParameter().setName("host").setValue("localhost");
    // wrong parse
    const char* argsWrong[] = {"./test", "--host=mail.ru", "--host", "127.0.0.1"};
    ASSERT_THROW( conf2.parse(4, argsWrong), std::logic_error );
}

TEST(configure, singletonFlags)
{
    // configure
    spm::Tuning confA;
    confA.makeFlag().setName('S').setName("sort");
    confA.makeParameter().setName("format").setName('f').setMultiValues();
    confA.makeOperand().setName("files").setMultiValues();
    // testing
    const char* argsA[] = {"./test", "--sort", "-fpng", "--", "a.jpg", "b.jpg"};
    ASSERT_NO_THROW( confA.parse(6, argsA) );
    // configure
    spm::Tuning confB;
    confB.makeFlag().setName('S').setName("sort");
    confB.makeParameter().setName("format").setName('f').setMultiValues();
    confB.makeOperand().setName("files").setMultiValues();
    // testing
    const char* argsB[] = {"./test", "--sort", "-fpng", "-S", "--", "a.jpg", "b.jpg"};
    ASSERT_THROW( confB.parse(7, argsB), std::logic_error );
    // configure
    spm::Tuning confC;
    confC.makeFlag().setName('S').setName("sort");
    confC.makeParameter().setName("format").setName('f').setMultiValues();
    confC.makeOperand().setName("files").setMultiValues();
    // testing
    const char* argsC[] = {"./test", "-fpng", "-f", "bmp", "-S", "--", "a.jpg", "b.jpg"};
    ASSERT_NO_THROW( confC.parse(8, argsC) );
}

TEST(configure, operands)
{
    // configure
    spm::Tuning conf;
    conf.makeFlag().setName('S').setName("ssl");
    conf.makeParameter().setName("protocol").setName('p').setValue("TCP");
    conf.makeOperand().setName("address").setValue("127.0.0.1");
    conf.makeOperand().setName("port").setValue("1234");
    conf.makeOperand().setName("files").setMultiValues();
    // testing
    const char* args[] = {"./test", "--ssl", "-pUDP", "google.com", "8080", "a.png", "b.jpg", "c.mp3"};
    ASSERT_NO_THROW( conf.parse(8, args) );
    ASSERT_EQ( conf.hasFlag("ssl"), true );
    ASSERT_EQ( conf.getParameter("protocol").asString(), "UDP" );
    ASSERT_EQ( conf.getOperand("address").asString(), "google.com" );
    ASSERT_EQ( conf.getOperand("port").asString(), "8080" );
    ASSERT_EQ( conf.getOperand("files").getValueList(), spm::Parameter::ValueList({"a.png", "b.jpg", "c.mp3"}) );
}

TEST(configure, help)
{
    // configure
    spm::Tuning conf;
    conf.makeParameter().setName('a').setName("address")
            .setValue("localhost").setDescription("used ip-address (default is localhost)");
    conf.makeFlag().setName("debug");       // hidden flag for debug
    conf.makeOperand().setName("files").setMultiValues().setDescription("files for sending");
    // testing
    const char* args[] = {"./test"};
    const char* helpMsg = "Usage: test [OPTION]... [files]...\n"
                          "  program for sending files\n"
                          "Description of operands:\n"
                          "  files - files for sending\n"
                          "Description of options:\n"
                          "  -a, --address\n"
                          "    used ip-address (default is localhost)\n";
    conf.parse(1, args);
    // conf.printHelp("program for sending files");
    std::stringstream buffer;
    conf.printHelp("program for sending files", buffer);
    ASSERT_EQ( buffer.str(), helpMsg );
}

TEST(configure, checker)
{
    // === A ===
    // configure
    spm::Tuning confA;
    confA.makeParameter().setName("address").setCheckerValue("^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
                                                             "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])$");
    // testing
    const char* argsA[] = {"./test", "--address=127.0.0.1"};
    ASSERT_NO_THROW( confA.parse(2, argsA) );
    ASSERT_EQ( confA.getParameter("address").asString(), "127.0.0.1" );
    // === B ===
    // configure
    spm::Tuning confB;
    confB.makeParameter().setName("address").setCheckerValue("^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
                                                             "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])$");
    // testing
    const char* argsB[] = {"./test", "--address", "0.255.255.255"};
    ASSERT_NO_THROW( confB.parse(3, argsB) );
    ASSERT_EQ( confB.getParameter("address").asString(), "0.255.255.255" );
    // === C ===
    // configure
    spm::Tuning confC;
    confC.makeParameter().setName("address").setCheckerValue("^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
                                                             "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])$");
    // testing
    const char* argsC[] = {"./test", "--address", "256.168.0.0"};
    ASSERT_THROW( confC.parse(3, argsC), std::logic_error );

}
