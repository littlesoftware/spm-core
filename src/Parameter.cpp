#include "Parameter.h"
#include "Tuning.h"
#include <algorithm>

using namespace spm;

Parameter::Parameter(Tuning *manager, bool isFlag):
    m_manager(manager),
    m_isFlag(isFlag),
    m_isOperand(false),
    m_isSingleton(true),
    m_settedValue(false),
    m_descValue("VALUE"),
    m_useRegEx(false)
{
}

Parameter::Parameter(Tuning *manager):
    m_manager(manager),
    m_isOperand(true),
    m_isSingleton(true),
    m_settedValue(false),
    m_useRegEx(false)
{
}

Parameter &Parameter::setName(char name)
{
    return setName(std::string(1,name));
}

Parameter &Parameter::setName(const std::string &name)
{
    if(m_isOperand)
        m_manager->setNameOperand(name, this);
    else
        m_manager->setNameOption(name, this);
    return *this;
}

Parameter &Parameter::setDescription(const std::string &description)
{
    m_description = description;
    return *this;
}

Parameter &Parameter::setValue(const std::string &value, int &isOk)
{
    if( m_useRegEx && !std::regex_match(value, m_regexChecker) )
    {
        isOk = 2;
        return *this;
    }
    if( m_isSingleton )
    {
        if( m_manager->m_settedMode )
        {
            if( m_settedValue )
            {
                isOk = 1;
                return *this;
            }
            m_settedValue = true;
        }
        m_value = value;
    }
    else
    {
        m_values.push_back(value);
    }
    isOk = 0;
    return *this;
}

Parameter &Parameter::setValue(const std::string &value)
{
    m_value = value;
    return *this;
}

bool Parameter::isFlag() const
{
    return m_isFlag;
}

const std::string &Parameter::asString() const
{
    return m_value;
}

bool Parameter::asBoolean() const
{
    std::string value = m_value;
    std::transform(m_value.begin(), m_value.end(), value.begin(), ::tolower);
    return !value.empty() && value != "0" && value != "false" && value != "no" && value != "n";
}

int Parameter::asInteger() const
{
    return atoi(m_value.c_str());
}

Parameter &Parameter::setMultiValues(bool singleton)
{
    m_isSingleton = singleton;
    return *this;
}

const Parameter::ValueList &Parameter::getValueList() const
{
    return m_values;
}

const std::string &Parameter::getDescription() const
{
    return m_description;
}

bool Parameter::isSingleton() const
{
    return m_isSingleton;
}

Parameter &Parameter::setCheckerValue(const std::string &regex)
{
    m_regexChecker = regex;
    m_useRegEx = true;
    return *this;
}
