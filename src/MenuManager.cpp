#include "MenuManager.h"

using namespace spm;

MenuManager::MenuManager(IMenuListener *listener):
    m_listener(listener)
{
}

void MenuManager::addMenu(int type, const Menu &menu)
{
    m_menus[type] = menu;
}

void MenuManager::showMenu(int type)
{
    if(m_menus.find(type) == m_menus.end())
        return;
    // show menu
    m_stack.push(&m_menus[type]);
    m_listener->onOpenMenu(m_stack.top());
}

void MenuManager::selectItemMenu(int index)
{
    // test index
    if(m_stack.empty())
        return;
    Menu* menu = m_stack.top();
    Menu* submenu = menu->getSubmenuByIndex(index);
    if(submenu == nullptr)
        return;
    // if menu has a submenu or
    // if has no submenu but variable
    if(!submenu->isEmpty() || submenu->isVariable())
    {
        // open sub menu
        m_stack.push(submenu);
        m_listener->onOpenMenu(submenu);
        return;
    }
    // no submenu
    // perform actions
    for(uint32_t type : submenu->getActions())
    {
        switch (type) {
        case Action::NONE:
            break;
        case Action::CLOSE_MENU:
            // menu is variable
            if(menu->isVariable())
            {
                //menu->setValueByIndex(index);
            }
            m_stack.pop();
            m_listener->onCloseMenu();
            break;
        default:
            m_listener->onMenuAction(type);
            break;
        }
    }
}

void MenuManager::cancelMenu()
{
    if(m_stack.empty())
        return;
    m_stack.pop();
    m_listener->onCloseMenu();
}

