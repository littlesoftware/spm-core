#include "Logger.h"

#include <iomanip>
#include <chrono>
#include <thread>

#if defined(_WIN32)
#include <windows.h>
#undef ERROR
#endif

using namespace spm;

std::mutex Logger::_mutex;
Logger::Settings Logger::m_settings;

Logger::Logger(const std::string& name):
    _name(name)
{
    beginSegment();
}

Logger::~Logger()
{
    if(_segment)
        endSegment();
}

void Logger::setLowType(Type type)
{
    m_settings.lowType = type;
}

void Logger::setOutput(std::ostream &output)
{
    m_settings.output = &output;
}

void Logger::setOutput(std::ostream &output, std::ostream &error)
{
    m_settings.output = &output;
    m_settings.error  = &error;
}

void Logger::setOutputError(std::ostream &error)
{
    m_settings.error = &error;
}

void Logger::addFlag(uint32_t flag)
{
    m_settings.flags |= flag;
}

void Logger::removeFlag(uint32_t flag)
{
    m_settings.flags &= ~flag;
}

void Logger::setFlags(uint32_t flags)
{
    m_settings.flags = flags;
}

bool Logger::redirectToFile(const std::string &filename, bool andError, bool append)
{
    m_settings.outputFile.open( filename, std::ios_base::out | (append?std::ios_base::app:std::ios_base::trunc) );
    if( !m_settings.outputFile.is_open() )
        return false;
    if(andError)
        setOutput(m_settings.outputFile, m_settings.outputFile);
    else
        setOutput(m_settings.outputFile);
    return true;
}

bool Logger::redirectErrorToFile(const std::string &filename, bool append)
{
    m_settings.errorFile.open( filename, std::ios_base::out | (append?std::ios_base::app:std::ios_base::trunc) );
    if( !m_settings.outputFile.is_open() )
        return false;
    setOutputError(m_settings.outputFile);
    return true;
}

Logger::Logger():
    _segment(false),
    _name("System")
{
}

Logger &Logger::system()
{
    static Logger logger;
    return logger;
}

void Logger::makeLog(const std::string& message, const Type type)
{
    if(type < m_settings.lowType)
        return;
    std::ostringstream output;
    bool hasColor = m_settings.flags & LoggerFlag::COLOR;

    // print header
    if( m_settings.flags & LoggerFlag::DATETIME )
        printDateTime(output);
    if( m_settings.flags & LoggerFlag::THREAD )
        printThreadId(output);
    if( m_settings.flags & LoggerFlag::NAME )
        output << _name << " ";
    if( m_settings.flags & LoggerFlag::TYPE )
        output << stringOfLevel(type) << " ";

    if( m_settings.flags & LoggerFlag::SEPARATION )
        output << ": " << message;
    else
        output << message;

    // print message
    {
        std::lock_guard<std::mutex> lockOutput(_mutex);
        std::ostream& stream = type>=Type::ERROR ? *m_settings.error : *m_settings.output;
        // if has coloring
        if(hasColor)
        {
            printColorOfType(stream, type);
            stream << output.str();
            printResetColor(stream);
            stream << std::endl;
        } else {
            stream << output.str() << std::endl;
        }
    }

    if(type >= Type::CRITICAL)
    {
        m_settings.output[0] << std::flush;
        m_settings.error[0] << std::flush;
    }
}

void Logger::beginSegment()
{
    makeLog("begin", Type::SEGMENT);
}

void Logger::endSegment()
{
    makeLog("end", Type::SEGMENT);
}

const char* Logger::stringOfLevel(Type type)
{
    static const char* levelStrings[] = {"T", "+", "D", "I", "W", "E", "C"};
    return levelStrings[static_cast<uint32_t>(type)];
}

void Logger::printColorOfType(std::ostream &os, Type type)
{
#if defined(__unix__)
    static const char* colorStrings[] = {"\033[0;37m",
                                         "\033[0;36m",
                                         "\033[1;37m",
                                         "\033[1;32m",
                                         "\033[1;33m",
                                         "\033[1;31m",
                                         "\033[1;37m\033[0;41m"};
    os << colorStrings[static_cast<uint32_t>(type)];
#elif defined(_WIN32)
    static WORD colorWords[] = {0x08, 0x0B, 0x0F, 0x0A, 0x0E, 0x0C, 0xCF};
    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),colorWords[static_cast<uint32_t>(type)]);
#endif
}

void Logger::printResetColor(std::ostream &os)
{
#if defined(__unix__)
    os << "\033[0m";
#elif defined(_WIN32)
    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),0x0F);
#endif
}

void Logger::printDateTime(std::ostream &os)
{
    // get time structures
    auto currentTime = std::chrono::system_clock::now();
    std::time_t time = std::chrono::system_clock::to_time_t(currentTime);
    ::tm* ptrTm = localtime(&time);
    // buffer for write date/time
    char buffer[100];
    if( m_settings.flags & LoggerFlag::DATE )
    {
        ::strftime(buffer, 100, "%Y-%m-%d", ptrTm);
        os << buffer << " ";
    }
    if( m_settings.flags & LoggerFlag::TIME )
    {
        ::strftime(buffer, 100, "%H:%M:%S", ptrTm);
        os << buffer;
        if( m_settings.flags & LoggerFlag::MILLISECONS )
        {
            int milliseconds = currentTime.time_since_epoch().count() / 1000000 % 1000;
            os << "." << std::setw(3) << std::setfill('0') << milliseconds << " ";
        }
        else
            os << " ";
    }
}

void Logger::printThreadId(std::ostream &os)
{
    os << "[" << std::hex << std::this_thread::get_id() << "] ";
}
