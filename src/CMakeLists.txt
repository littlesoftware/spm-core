cmake_minimum_required(VERSION 3.5)

project(spm-core LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

option(BUILD_SHARED      "Build the shared library" OFF)

include_directories( ${PROJECT_SOURCE_DIR}/../include )

set( SOURCES
    Tuning.cpp
    Parameter.cpp
    Variant.cpp
    Serializer.cpp
    utils.cpp
    ServiceManager.cpp
    BaseService.cpp
    Menu.cpp
    Logger.cpp
    MenuManager.cpp
)

set( HEADERS
)

file(GLOB GLOBAL_HEADERS
     "${PROJECT_SOURCE_DIR}/../include/*.h"
)

set(LIBRARY_TYPE STATIC)
if(BUILD_SHARED)
    set(LIBRARY_TYPE SHARED)
endif(BUILD_SHARED)
add_library(${PROJECT_NAME} ${LIBRARY_TYPE} ${SOURCES} ${HEADERS} ${GLOBAL_HEADERS})

