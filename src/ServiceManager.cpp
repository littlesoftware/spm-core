#include "ServiceManager.h"
#include <iostream>

using namespace spm;

ServiceManager::ServiceManager()
{
}

ServiceManager::~ServiceManager()
{
}

void ServiceManager::addService(IService *service)
{
    if(service == nullptr)
        return;
    IServicePtr ptr(service);
    m_services.insert({service->getServiceName(), ptr});
    m_activeTicks.insert(ptr);
}

bool ServiceManager::hasService(const std::string &service) const
{
    return m_services.find(service) != m_services.end();
}

bool ServiceManager::performRequest(const std::string &service, const std::string &type, const Variant &args, Variant &result) const
{
    bool success = true;
    try
    {
        Variant data;
        success = m_services.at(service)->request(type, args, data);
        result = success ?
                    Variant::Dict({
                                      {"error", false},
                                      {"answer", std::move(data)}
                               }) :
                    Variant::Dict({
                                      {"error", true},
                                      {"message", "bad result"}
                                  });
    }
    catch(const std::exception& e)
    {
        result = Variant::Dict({
                                   {"error", true},
                                   {"massage", e.what()}
                               });
        success = false;
    }
    catch(...)
    {
        result = Variant::Dict({
                                   {"error", true},
                                   {"massage", "unknown error"}
                               });
        success = false;
    }
    return success;
}

void ServiceManager::performAction(const std::string &type, const Variant &args) const
{
    auto range = m_supportedTypes.equal_range(type);
    for(auto it=range.first; it!=range.second; it++)
    {
        try
        {
            it->second->action(type, args);
        }
        catch(const std::exception& e)
        {
            // to do some logs?
        }
        catch(...)
        {
            // to do some logs?
        }
    }
}

bool ServiceManager::run()
{
    initialize();
    // start loop
    while(!m_activeTicks.empty())
    {
        std::set<IServicePtr> toRemoveTicks;
        for(auto& tick : m_activeTicks)
        {
            if(!tick->tick())
                toRemoveTicks.insert(tick);
        }
        if(!toRemoveTicks.empty())
            for(auto& tick : toRemoveTicks)
                m_activeTicks.erase(tick);
    }
    return true;
}

void ServiceManager::initialize()
{
    // get supported type
    for(auto& pair : m_services)
    {
        TypesCollection collection;
        pair.second->getSupportedTypes(collection);
        for(auto& type : collection)
        {
            m_supportedTypes.insert({type, pair.second});
        }
    }
    // initialized services
    for(auto& pair : m_services)
        pair.second->initialize();
}
