#include "BaseService.h"

using namespace spm;

BaseService::BaseService(const std::string &serviceName):
    m_serviceName(serviceName)
{
}

const std::string &BaseService::getServiceName() const
{
    return m_serviceName;
}

void BaseService::getSupportedTypes(TypesCollection &list) const
{
    list = m_supportedTypes;
}

bool BaseService::request(const std::string&, const Variant&, Variant&)
{
    return false;
}

void BaseService::action(const std::string&, const Variant&)
{
}

void BaseService::initialize()
{
}

bool BaseService::tick()
{
    return false;
}

void BaseService::addSupportedType(const std::string &type)
{
    m_supportedTypes.insert(type);
}
