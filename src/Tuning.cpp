#include "Tuning.h"
#include <stdio.h>
#include <regex>
#include <fstream>
#include <cstdio>
#include <stdarg.h>

using namespace spm;

Tuning::Tuning():
    m_settedMode(false)
{
}

Parameter &Tuning::makeFlag()
{
    ParameterPtr param(new Parameter(this, true));
    m_parameters.insert({param.get(), param});
    return param.get()[0];
}

Parameter &Tuning::makeParameter()
{
    ParameterPtr param(new Parameter(this, false));
    m_parameters.insert({param.get(), param});
    return param.get()[0];
}

Parameter &Tuning::makeOperand()
{
    ParameterPtr param(new Parameter(this));
    m_parameters.insert({param.get(), param});
    return param.get()[0];
}

void Tuning::parse(std::istream &stream)
{
    std::string line;
    std::regex regexParameter("(.+)=(.*)");
    std::regex regexSection("[[:space:]]*\\[(.+)\\][[:space:]]*");
    bool sectionTuning = false;
    // read all stream
    while(std::getline(stream, line))
    {
        std::smatch sm;
        // trim comment
        line = trimComment(line);
        // is parameter
        if(std::regex_match(line, sm, regexParameter))
        {
            std::string key = trimSpace(sm[1]);
            std::string value = trimSpace(sm[2]);
            if(sectionTuning && m_namedParameters.find(key) != m_namedParameters.end())
                m_namedParameters[key]->setValue(value);
            continue;
        }
        // is section
        if(std::regex_match(line, sm, regexSection))
        {
            std::string name = trimSpace(sm[1]);
            sectionTuning = (name == "tuning");
            continue;
        }
    }
}

void Tuning::parse(const std::string &filename)
{
    std::ifstream file;
    file.open(filename);
    if(!file.is_open())
        return;
    parse(file);
    file.close();
}

void Tuning::parse(int argc, const char *argv[])
{
    if(argc > 0)
        m_programName = parseProgramName(argv[0]);
    if(argc < 2)
        return;
    m_settedMode = true;
    int isOk;
    for(int n=1; n<argc; n++)
    {
        int argLen = strlen(argv[n]);
        ParameterPtr parameter;
        // if long name parameter
        if( argLen > 2 && argv[n][0] == '-' && argv[n][1] == '-' )
        {
            std::string name = argv[n]+2;
            std::string value;
            auto parPos = name.find_first_of('=');
            bool isPar = parPos != std::string::npos;
            if( isPar )
            {
                value = name.substr(parPos+1);
                name = name.substr(0, parPos);
            }
            parameter = getParameterByName(name);
            if( isPar )
            {
            }
            else if( parameter->isFlag() )
            {
                value = "yes";
            }
            else
            {
                n++;
                if( n >= argc )
                    throw generateLogicError("the '--%s' parameter requires an argument", name.c_str());
                value = argv[n];
            }
            parameter->setValue(value, isOk);
            if( isOk == 0 )
                continue;
            else if( isOk == 1 )
                throw generateLogicError("multiple definition of the '--%s' parameter is not allowed", name.c_str());
            else if( isOk == 2 )
                throw generateLogicError("the '--%s' parameter setted invalid value '%s'", name.c_str(), value.c_str());
            continue;
        }
        // if short name parameter
        if( argLen > 1 && argv[n][0] == '-' && argv[n][1] != '-' )
        {
            const char* sym = argv[n];
            std::string value;
            while((++sym)[0])
            {
                parameter = getParameterByName(sym[0]);
                if( parameter->isFlag() )
                {
                    value = "yes";
                    parameter->setValue(value, isOk);
                    if( !isOk )
                        break;
                }
                else if(sym[1])
                {
                    value = sym+1;
                    parameter->setValue(value, isOk);
                    break;
                }
                else
                {
                    n++;
                    if( n >= argc )
                        throw generateLogicError("the key must be used with an argument - '%c'", sym[0]);
                    value = argv[n];
                    parameter->setValue(value, isOk);
                    break;
                }
            }
            if( isOk == 0 )
                continue;
            else if( isOk == 1 )
                throw generateLogicError("multiple definition of key is not allowed - '%c'", sym[0]);
            else if( isOk == 2 )
                throw generateLogicError("invalid value '%s' of key - '%c'", value.c_str(), sym[0]);
            continue;
        }
        // end of parameters & flags
        if( strcmp(argv[n],"--") == 0 )
            n++;
        // next is position parameters
        for(auto& name : m_operands)
        {
            if( n>=argc )
                break;
            m_namedOperands[name]->setValue(argv[n], isOk);
            if( isOk != 0 )
                break;
            n++;
        }
        // the last operand is duplicate
        if( isOk == 0 && !m_operands.empty() )
        {
            ParameterPtr parameter = m_namedOperands[m_operands.back()];
            while( n<argc )
            {
                parameter->setValue(argv[n], isOk);
                if( isOk != 0 )
                    break;
                n++;
            }
        }
        if( isOk != 0 )
            throw generateLogicError("invalid operand '%s'", argv[n]);
        break;
    }
    m_settedMode = false;
}

const Parameter &Tuning::getParameter(const std::string &name) const
{
    return m_namedParameters.at(name).get()[0];
}

const Parameter &Tuning::getParameter(char name) const
{
    return m_namedParameters.at(std::string(1,name)).get()[0];
}

const bool Tuning::hasFlag(const std::string &name) const
{
    return m_namedParameters.at(name)->asBoolean();
}

const bool Tuning::hasFlag(const char name) const
{
    return m_namedParameters.at(std::string(1,name))->asBoolean();
}

const Parameter &Tuning::getOperand(const std::string &name) const
{
    return m_namedOperands.at(name).get()[0];
}

const Parameter &Tuning::getOperand(char name) const
{
    return m_namedOperands.at(std::string(1,name)).get()[0];
}

void Tuning::printHelp(const std::string &description, std::ostream &output) const
{
    // general description
    output << "Usage: " << m_programName;
    if( !m_namedParameters.empty() )
        output << " " << "[OPTION]...";
    for( auto& operand : m_operands )
        output << " [" << operand << "]";
    if( !m_operands.empty() && !m_namedOperands.at(m_operands.back())->isSingleton() )
        output << "...";
    output << std::endl;
    output << "  " << description << std::endl;
    // description of operands
    if( !m_operands.empty() )
    {
        bool hasDescription = false;
        for( auto& operand : m_operands )
            if( !m_namedOperands.at(operand)->getDescription().empty() )
            {
                hasDescription = true;
                break;
            }
        if( hasDescription )
        {
            output << "Description of operands:" << std::endl;
            for( auto& operand : m_operands )
            {
                auto desc = m_namedOperands.at(operand)->getDescription();
                if( desc.empty() )
                    continue;
                output << "  " << operand << " - " << desc << std::endl;
            }
        }
    }
    // description of options
    if( !m_namedParameters.empty() )
    {
        std::map<ParameterPtr, std::list<std::string>> parameters;
        for( auto& pair : m_namedParameters )
        {
            if( pair.second->getDescription().empty() )
                continue;
            if( pair.first.size() == 1 )
                parameters[pair.second].push_front(pair.first);
            else
                parameters[pair.second].push_back(pair.first);
        }
        if( !parameters.empty() )
            output << "Description of options:" << std::endl;
        for( auto& pair : parameters )
        {
            bool isFirst = true;
            output << " ";
            for( auto& name : pair.second )
            {
                if( isFirst )
                    isFirst = false;
                else
                    output << ",";
                output << " " << (name.size() == 1 ? "-" : "--") << name;
            }
            output << std::endl << "    " << pair.first->getDescription() << std::endl;
        }
    }
}

const std::string &Tuning::getProgramName() const
{
    return m_programName;
}

void Tuning::setNameOption(const std::string &name, Parameter *parameter)
{
    m_namedParameters.insert({name, m_parameters.at(parameter)});
}

void Tuning::setNameOperand(const std::string &name, Parameter *parameter)
{
    m_namedOperands.insert({name, m_parameters.at(parameter)});
    m_operands.push_back(name);
}

std::string Tuning::trimComment(const std::string &str) const
{
    auto pos = str.find_first_of(";");
    if(pos == std::string::npos)
        return str;
    return str.substr(0,pos);
}

std::string Tuning::trimSpace(const std::string &str) const
{
    auto begin = str.find_first_not_of(" \t");
    if(begin == std::string::npos)
        return "";
    auto end = str.find_last_not_of(" \t");
    return str.substr(begin, end-begin+1);
}

std::string Tuning::parseProgramName(const char *name) const
{
    std::string res = name;
    // replace \ to /
    std::replace(res.begin(), res.end(), '\\', '/');
    auto pos = res.find_last_of("/");
    return res.substr(pos+1);
}

Tuning::ParameterPtr Tuning::getParameterByName(const std::string &name) const
{
    if(m_namedParameters.find(name) == m_namedParameters.end())
    {
        if( name.size() == 1 )
            throw generateLogicError("invalid key - '%s'", name.c_str());
        else
            throw generateLogicError("unrecognized option '--%s'", name.c_str());
    }
    return m_namedParameters.at(name);
}

Tuning::ParameterPtr Tuning::getParameterByName(char name) const
{
    return getParameterByName(std::string(1, name));
}

std::logic_error Tuning::generateLogicError(const char *fmt, ...) const
{
    va_list vl;
    va_list vlCopy;
    std::vector<char> data;
    std::string msg;
    va_start(vl, fmt);
    // get formated string
    va_copy(vlCopy, vl);
    int size = vsnprintf(nullptr, 0, fmt, vlCopy)+1;
    va_end(vlCopy);
    data.resize(size);
    int bytes = vsnprintf(data.data(), size, fmt, vl);
    if(bytes > 0)
        msg = data.data();
    // ---
    va_end(vl);
    return std::logic_error(msg);
}

std::logic_error Tuning::generateLogicError(const char *fmt, ...)
{
    va_list vl;
    va_list vlCopy;
    std::vector<char> data;
    std::string msg;
    va_start(vl, fmt);
    // get formated string
    va_copy(vlCopy, vl);
    int size = vsnprintf(nullptr, 0, fmt, vlCopy)+1;
    va_end(vlCopy);
    data.resize(size);
    int bytes = vsnprintf(data.data(), size, fmt, vl);
    if(bytes > 0)
        msg = data.data();
    // ---
    va_end(vl);
    m_settedMode = false;
    return std::logic_error(msg);
}
