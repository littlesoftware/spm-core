#include "Serializer.h"
#include <sstream>
#include <cstring>
#include <iomanip>

using namespace spm;

bool Serializer::dumpToBinary(const Variant &object, Variant::Data &data)
{
    switch (object.getType()) {
    case Variant::UNKNOWN:
        data.push_back(0);
        break;
    case Variant::INTEGER:
    {
        Variant::Integer value = object.toInteger();
        data.resize( 1 + sizeof(Variant::Integer) );
        data[0] = 1;
        memcpy(&data[1], &value, sizeof(Variant::Integer));
    }
        break;
    case Variant::STRING:
    {
        const Variant::String& value = object.toString();
        size_t sz = value.size();
        Variant::Data sizeData = sizeToDataBlock(sz);
        data.reserve( 1 + sizeData.size() + sz );
        data.push_back(2);
        data.insert(data.end(), sizeData.begin(), sizeData.end());
        Variant::Data strData = Variant(value.c_str(), sz).toData();
        data.insert(data.end(), strData.begin(), strData.end());
    }
        break;
    case Variant::REAL:
    {
        Variant::Real value = object.toReal();
        data.resize( 1 + sizeof(Variant::Real) );
        data[0] = 3;
        memcpy(&data[1], &value, sizeof(Variant::Real));
    }
        break;
    case Variant::BOOLEAN:
        data.resize( 2 );
        data[0] = 4;
        data[1] = object.toBoolean() ? 1 : 0;
        break;
    case Variant::LIST:
    {
        const Variant::List& value = object.toList();
        std::vector<Variant::Data> datas;
        size_t totalSize = 0;
        size_t listSize = value.size();
        datas.resize(listSize + 1);
        // data of size
        datas[0] = sizeToDataBlock(listSize);
        totalSize += datas[0].size();
        size_t index = 0;
        for(auto& obj : value)
        {
            index++;
            if(!dumpToBinary(obj, datas[index]))
                return false;
            totalSize += datas[index].size();
        }
        // join blocks
        data.reserve(totalSize + 1);
        data.push_back(5);
        for(auto& block : datas)
        {
            data.insert(data.end(), block.begin(), block.end());
        }
    }
        break;
    case Variant::ARRAY:
    {
        const Variant::Array& value = object.toArray();
        std::vector<Variant::Data> datas;
        size_t totalSize = 0;
        size_t arraySize = value.size();
        datas.resize(arraySize + 1);
        // data of size
        datas[0] = sizeToDataBlock(arraySize);
        totalSize += datas[0].size();
        size_t index = 0;
        for(auto& obj : value)
        {
            index++;
            // data of variant
            if(!dumpToBinary(obj, datas[index]))
                return false;
            totalSize += datas[index].size();
        }
        // join blocks
        data.reserve(totalSize + 1);
        data.push_back(6);
        for(auto& block : datas)
        {
            data.insert(data.end(), block.begin(), block.end());
        }
    }
        break;
    case Variant::DICT:
    {
        const Variant::Dict& value = object.toDict();
        std::vector<Variant::Data> datas;
        size_t totalSize = 0;
        size_t dictSize = value.size();
        datas.resize(3*dictSize + 1);
        datas[0] = sizeToDataBlock(dictSize);
        totalSize += datas[0].size();
        size_t index = 0;
        for(auto& pair : value)
        {
            // string of key
            size_t sz = pair.first.size();
            datas[1 + index] = sizeToDataBlock(sz);
            datas[2 + index].resize(sz);
            memcpy(datas[2 + index].data(), pair.first.c_str(), sz);
            // data of variant
            if(!dumpToBinary(pair.second, datas[3 + index]))
                return false;
            // add index and total size
            totalSize += datas[1 + index].size() + sz + datas[3 + index].size();
            index += 3;
        }
        // join blocks
        data.reserve(totalSize+1);
        data.push_back(7);
        for(auto& block : datas)
        {
            data.insert(data.end(), block.begin(), block.end());
        }
    }
        break;
    case Variant::DATA:
    {
        const Variant::Data& value = object.toData();
        Variant::Data blockSize;
        size_t totalSize = 0;
        size_t dataSize = value.size();
        blockSize = sizeToDataBlock(dataSize);
        totalSize = blockSize.size() + dataSize;
        // join
        data.reserve(totalSize + 1);
        data.push_back(8);
        data.insert(data.end(), blockSize.begin(), blockSize.end());
        data.insert(data.end(), value.begin(), value.end());
    }
        break;
    default:
        return false;
        break;
    }
    return true;
}

bool Serializer::loadFromBinary(const Variant::Data &data, Variant &object)
{
    return loadFromBinary(data.data(), data.size(), object) > 0;
}

bool Serializer::dumpToPlainText(const Variant &object, std::string &text)
{
    std::stringstream ss;
    if(!dumpPlainTextToStream(object, ss, ""))
        return false;
    text = ss.str();
    return true;
}

bool Serializer::loadFromPlainText(const std::string &str, Variant &object)
{
    std::stringstream ss;
    ss << str;
    return loadPlainTextFromStream(ss, object);
}

Variant::Data Serializer::sizeToDataBlock(size_t size)
{
    Variant::Data data;
    do
    {
        uint8_t byte = size>=0x80?0x80:0x00;
        byte |= size & 0x7F;
        size >>= 7;
        data.push_back(byte);
    }
    while(size > 0);
    return data;
}

size_t Serializer::dataBlockToSize(const void *data, int &bytes)
{
    size_t result = 0;
    const uint8_t* byte = static_cast<const uint8_t*>(data);
    bool highBit;
    bytes = 0;
    do
    {
        bytes++;
        highBit = byte[0] & 0x80;
        result <<= 7;
        result |= byte[0] & 0x7F;
        byte++;
    }
    while(highBit);
    return result;
}

size_t Serializer::loadFromBinary(const void *data, size_t size, Variant &object)
{
    const uint8_t* ptr = static_cast<const uint8_t*>(data);
    const uint8_t* ptrBegin = ptr;
    // read type
    if(size <= 0)
        return 0;
    uint8_t type = ptr[0];
    size--;
    ptr++;
    // switch type
    switch (type) {
    case 0: // unknown
        object.setUnknown();
        break;
    case 1: // integer
    {
        if(size < sizeof(Variant::Integer))
            return 0;
        object.setInteger(0);
        memcpy(&object.getInteger(), ptr, sizeof(Variant::Integer));
        ptr += sizeof(Variant::Integer);
    }
        break;
    case 2: // string
    {
        // read size
        int bytes;
        size_t len = dataBlockToSize(ptr, bytes);
        ptr += bytes;
        size -= bytes;
        // copy string
        if(size < len)
            return 0;
        object.setString("");
        Variant::String& value = object.getString();
        value.resize(len, 0);
        memcpy(&value[0], ptr, len);
        ptr += len;
    }
        break;
    case 3: // real
    {
        if(size < sizeof(Variant::Real))
            return 0;
        object.setReal(0);
        memcpy(&object.getReal(), ptr, sizeof(Variant::Real));
        ptr += sizeof(Variant::Real);
    }
        break;
    case 4: // boolean
    {
        if(size < 1)
            return 0;
        object.setBoolean(static_cast<const uint8_t*>(ptr)[0] != 0);
        ptr++;
        size--;
    }
        break;
    case 5: // list
    {
        // read size
        int bytes;
        size_t len = dataBlockToSize(ptr, bytes);
        ptr += bytes;
        size -= bytes;
        // read list
        object.setList({});
        Variant::List& value = object.getList();
        for(size_t i=0; i<len; i++)
        {
            Variant obj;
            size_t objSize = loadFromBinary(ptr, size, obj);
            if(objSize < 1)
                return 0;
            ptr += objSize;
            size -= objSize;
            value.push_back(std::move(obj));
        }
    }
        break;
    case 6: // array
    {
        // read size
        int bytes;
        size_t len = dataBlockToSize(ptr, bytes);
        ptr += bytes;
        size -= bytes;
        // read array
        object.setArray({});
        Variant::Array& value = object.getArray();
        value.reserve(len);
        for(size_t i=0; i<len; i++)
        {
            Variant obj;
            size_t objSize = loadFromBinary(ptr, size, obj);
            if(objSize < 1)
                return 0;
            ptr += objSize;
            size -= objSize;
            value.push_back(std::move(obj));
        }
    }
        break;
    case 7: // dict
    {
        // read size
        int bytes;
        size_t len = dataBlockToSize(ptr, bytes);
        ptr += bytes;
        size -= bytes;
        // read dict
        object.setDict({});
        Variant::Dict& value = object.getDict();
        for(size_t i=0; i<len; i++)
        {
            // read key
            int keySzBytes;
            size_t keyLen = dataBlockToSize(ptr, keySzBytes);
            ptr += keySzBytes;
            size -= keySzBytes;
            if(size < keyLen)
                return 0;
            std::string key;
            key.resize(keyLen, 0);
            memcpy(&key[0], ptr, keyLen);
            ptr += keyLen;
            size -= keyLen;
            // read value
            size_t objSize = loadFromBinary(ptr, size, value[key]);
            if(objSize < 1)
                return 0;
            ptr += objSize;
            size -= objSize;
        }
    }
        break;
    case 8: // data
    {
        // read size
        int bytes;
        size_t len = dataBlockToSize(ptr, bytes);
        ptr += bytes;
        size -= bytes;
        // read data
        if(size < len)
            return 0;
        object.setData({});
        Variant::Data& value = object.getData();
        value.resize(len);
        memcpy(value.data(), ptr, len);
        ptr += len;
    }
        break;
    default:
        return 0;
        break;
    }
    return ptr-ptrBegin;
}

bool Serializer::dumpPlainTextToStream(const Variant &object, std::ostream &output, const std::string &space)
{
    switch (object.getType()) {
    case Variant::UNKNOWN:
        output << space << "Unknown" << std::endl;
        break;
    case Variant::INTEGER:
        output << space << "Integer " << object.toInteger() << std::endl;
        break;
    case Variant::STRING:
    {
        const Variant::String& value = object.toString();
        size_t sz = value.size();
        output << space << "String " << sz << " ";
        output.write(value.c_str(), sz);
        output << std::endl;
    }
        break;
    case Variant::REAL:
        output.setf(std::ios_base::fixed);
        output << space << "Real " << std::setprecision(10) << object.toReal() << std::endl;
        output.unsetf(std::ios_base::fixed);
        break;
    case Variant::BOOLEAN:
        output << space << "Boolean " << object.toBoolean() << std::endl;
        break;
    case Variant::LIST:
    {
        const Variant::List& value = object.toList();
        output << space << "List " << value.size() << std::endl;
        for(auto& it : value)
        {
            if(!dumpPlainTextToStream(it, output, space + "  "))
                return false;
        }
    }
        break;
    case Variant::ARRAY:
    {
        const Variant::Array& value = object.toArray();
        output << space << "Array " << value.size() << std::endl;
        for(auto& it : value)
        {
            if(!dumpPlainTextToStream(it, output, space + "  "))
                return false;
        }
    }
        break;
    case Variant::DICT:
    {
        const Variant::Dict& value = object.toDict();
        output << space << "Dict " << value.size() << std::endl;
        for(auto& it : value)
        {
            size_t sz = it.first.size();
            output << space << sz << " ";
            output.write(it.first.c_str(), sz) << " :" << std::endl;
            if(!dumpPlainTextToStream(it.second, output, space + "  "))
                return false;
        }
    }
        break;
    case Variant::DATA:
    {
        const Variant::Data& value = object.toData();
        output << space << "Data " << value.size() << " ";
        // print to hex bytes
        const char* characters = "0123456789ABCDEF";
        for(auto byte : value)
        {
            output << characters[byte>>4] << characters[byte&0x0F];
        }
        output << std::endl;
    }
        break;
    default:
        return false;
        break;
    }
    return true;
}

bool Serializer::loadPlainTextFromStream(std::istream &stream, Variant &object)
{
    std::string type;
    stream >> type;
    if(type == "Unknown")
    {
        object = Variant();
    }
    else if(type == "Integer")
    {
        Variant::Integer value;
        stream >> value;
        object = value;
    }
    else if(type == "String")
    {
        // initialize as String and get reference
        object.setString("");
        Variant::String& value = object.getString();
        size_t sz;
        stream >> sz;
        value.resize(sz, 0);
        // read space
        stream.get();
        // read string
        stream.read(&value[0], sz);
    }
    else if(type == "Real")
    {
        Variant::Real value;
        stream >> value;
        object = value;
    }
    else if(type == "Boolean")
    {
        Variant::Boolean value;
        stream >> value;
        object = value;
    }
    else if(type == "List")
    {
        // initialize as List and get reference
        object.setList({});
        Variant::List& value = object.getList();
        size_t sz;
        // read size
        stream >> sz;
        // append to list
        for(size_t i=0; i<sz; i++)
        {
            spm::Variant obj;
            // read value object
            if(!loadPlainTextFromStream(stream, obj))
            {
                object.setUnknown();
                return false;
            }
            // move object to list
            value.push_back(std::move(obj));
        }
    }
    else if(type == "Array")
    {
        // initialize as Array and get reference
        object.setArray({});
        Variant::Array& value = object.getArray();
        size_t sz;
        // read size
        stream >> sz;
        value.reserve(sz);
        // append to array
        for(size_t i=0; i<sz; i++)
        {
            spm::Variant obj;
            // read value object
            if(!loadPlainTextFromStream(stream, obj))
            {
                object.setUnknown();
                return false;
            }
            // move object to list
            value.push_back(std::move(obj));
        }
    }
    else if(type == "Dict")
    {
        // initialize as Dict and get reference
        object.setDict({});
        Variant::Dict& value = object.getDict();
        size_t sz;
        std::string sepKey;
        // read size of dictionary
        stream >> sz;
        for(size_t i=0; i<sz; i++)
        {
            // read key
            size_t szKey;
            std::string key;
            stream >> szKey;
            stream.get();
            key.resize(szKey, 0);
            stream.read(&key[0], szKey);
            // dict key to end ':'
            stream >> sepKey;
            if(sepKey != ":")
            {
                object.setUnknown();
                return false;
            }
            // read value
            if(!loadPlainTextFromStream(stream, value[key]))
            {
                object.setUnknown();
                return false;
            }
        }
    }
    else if(type == "Data")
    {
        // initialize as Data and get reference
        object.setData({});
        Variant::Data& value = object.getData();
        size_t sz;
        char buf[3] = {0,0,0};
        // read size of data
        stream >> sz;
        stream.get();   // read space
        value.reserve(sz);
        for(size_t i=0; i<sz; i++)
        {
            // read 2 characters
            stream.read(buf, 2);
            // decode to byte and add to data
            value.push_back( ((buf[0]-'0')<<4) | (buf[1]-'0') );
        }
    }
    else
        return false;
    return true;
}
