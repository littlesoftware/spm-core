#include "Menu.h"

using namespace spm;

Menu::Menu()
{
}

Menu::Menu(const std::string &name):
    m_name(name)
{
}

Menu::Menu(const std::string &name, const std::initializer_list<Menu> &menuList):
    m_name(name)
{
    for(auto& menu : menuList)
        addSubmenu(menu);
}

Menu::~Menu()
{
}

Menu &Menu::addSubmenu(const Menu &menu)
{
    m_menuList.push_back(menu);
    m_menuListCounter++;
    return *this;
}

Menu &Menu::setName(const std::string &name)
{
    m_name = name;
    return *this;
}

const std::string &Menu::getName() const
{
    return m_name;
}

Menu &Menu::addAction(uint32_t action)
{
    m_actions.push_back(action);
    return *this;
}

Menu &Menu::setVariable()
{
    m_isVariable = true;
    return *this;
}

bool Menu::isVariable() const
{
    return m_isVariable;
}

const Menu::ActionList &Menu::getActions() const
{
    return m_actions;
}

bool Menu::isEmpty() const
{
    return m_menuList.empty();
}

int Menu::getSize() const
{
    return m_menuList.size();
}

Menu::MenuListIterator Menu::begin() const
{
    return m_menuList.begin();
}

Menu::MenuListIterator Menu::end() const
{
    return m_menuList.end();
}

Menu *Menu::getSubmenuByIndex(int index)
{
    if(index < 0 || index >= m_menuList.size())
        return nullptr;
    return &m_menuList.at(index);
}

void Menu::setIndex(int index)
{
    m_index = index;
    if(m_menuList.empty())
    {
        m_index = -1;
        return;
    }
    if(m_index < 0)
    {
        m_index = 0;
    }
    else if(m_index >= m_menuList.size())
    {
        m_index = m_menuList.size() -1;
    }
    m_value = m_menuList.at(index).getName();
}

Menu &Menu::setValue(const std::string &value)
{
    m_value = value;
    return *this;
}

const std::string &Menu::getValue() const
{
    return m_value;
}
