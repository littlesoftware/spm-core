#include "utils.h"
#include <cstdio>
#include <cstdarg>
#include <vector>
#include <sstream>
#include <iomanip>

//using namespace spm;

std::string spm::stringf(const char *fmt, ...)
{
    va_list vl;
    va_start(vl, fmt);
    std::vector<char> data;
    va_list argcopy;
    // get string size
    va_copy(argcopy, vl);
    int size = vsnprintf(nullptr, 0, fmt, argcopy)+1;
    va_end(argcopy);
    // formatted string
    data.resize(size);
    int bytes = vsnprintf(data.data(), size, fmt, vl);
    va_end(vl);
    // return result
    if(bytes >= 0)
        return data.data();
    return "";
}

std::logic_error spm::exceptf(const char *fmt, ...)
{
    va_list vl;
    va_start(vl, fmt);
    std::vector<char> data;
    va_list argcopy;
    // get string size
    va_copy(argcopy, vl);
    int size = vsnprintf(nullptr, 0, fmt, argcopy)+1;
    va_end(argcopy);
    // formatted string
    data.resize(size);
    int bytes = vsnprintf(data.data(), size, fmt, vl);
    va_end(vl);
    // return result
    if(bytes >= 0)
        return std::logic_error(data.data());
    return std::logic_error("exception error");
}

std::string spm::dumpHex(const void *data, size_t size)
{
    std::stringstream ss;
    const uint8_t* ptr = static_cast<const uint8_t*>(data);
    size_t line = 0;
    char buff[17];
    buff[16] = 0;
    const char* characters = "0123456789abcdef";
    // print header
    ss << "         | 00 01 02 03 04 05 06 07  08 09 0a 0b 0c 0d 0e 0f |                 " << std::endl;
    ss << "---------+--------------------------------------------------+-----------------" << std::endl;
    do
    {
        // print line number
        ss << std::setfill('0') << std::setw(8) << std::hex << line << " | ";
        // print hex dump
        for(int i=0; i<16; i++)
        {
            if(size > 0)
            {
                size--;
                ss << characters[ptr[0]>>4] << characters[ptr[0]&0x0F] << " ";
                buff[i] = ptr[0] > 0x1f && ptr[0] < 0x7f ? ptr[0] : '.';
                ptr++;
            }
            else
            {
                ss << "   ";
                buff[i] = ' ';
            }
            if(i==7)
                ss << " ";
        }
        ss << "| " << buff << std::endl;
        // add new line
        line += 16;
    }
    while(size > 0);
    return ss.str();
}

size_t spm::stringLengthByUtf8(const std::string &text)
{
    const char* pointer = text.c_str();
    const uint8_t *ch = reinterpret_cast<const uint8_t*>(pointer);
    size_t length=0;

    while(ch[0] != 0)
    {
        // 0xxxxxxx
        if ((ch[0] & 0x80) == 0)
        {
            ch++;
        }
        // 110xxxxx 10xxxxxx
        else if ((ch[0] & 0xE0) == 0xC0)
        {
            ch+=2;
        }
        // 1110xxxx 10xxxxxx 10xxxxxx
        else if ((ch[0] &0xF0) == 0xE0)
        {
            ch+=3;
        }
        // 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx
        else
        {
            ch+=4;
        }
        length++;
    }
    return length;
}

std::wstring spm::wcast(const std::string &text)
{
    const char* pointer = text.c_str();
    const uint8_t *ch = reinterpret_cast<const uint8_t*>(pointer);
    std::wstring result;
    result.reserve(text.size());
    while(ch[0] != 0)
    {
        wchar_t code;
        // 0xxxxxxx
        if ((ch[0] & 0x80) == 0)
        {
            code = ch[0];
            ch++;
        }
        // 110xxxxx 10xxxxxx
        else if ((ch[0] & 0xE0) == 0xC0)
        {
            code = ((ch[0] & 0x1F) << 6) | (ch[1] & 0x3F);
            ch+=2;
        }
        // 1110xxxx 10xxxxxx 10xxxxxx
        else if ((ch[0] &0xF0) == 0xE0)
        {
            code = ((ch[0] & 0x0F) << 12) | ((ch[1] & 0x3F) << 6) | (ch[2] & 0x3F);
            ch+=3;
        }
        // 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx
        else
        {
            code = ((ch[0] & 0x07) << 18) | ((ch[1] & 0x3F) << 12) | ((ch[2] & 0x3F) << 6) | (ch[3] & 0x3F);
            ch+=4;
        }
        result += code;
    }
    return result;
}
