#include "Variant.h"
#include <sstream>
#include <cstring>

using namespace spm;

// empty constructor
Variant::Variant():
    m_type(UNKNOWN)
{
}

// constructor as integer type
Variant::Variant(int value):
    m_type(INTEGER),
    m_dataInteger(value)
{
}

Variant::Variant(unsigned int value):
    m_type(INTEGER),
    m_dataInteger(value)
{
}

// constructor as integer type
Variant::Variant(Integer value):
    m_type(INTEGER),
    m_dataInteger(value)
{
}

// constructor as boolean type
Variant::Variant(Boolean value):
    m_type(BOOLEAN),
    m_dataBoolean(value)
{
}

// constructor as string type
Variant::Variant(const char *value):
    m_type(STRING)
{
    m_dataString = new std::string;
    m_dataString[0] = value;
}

// constructor as string type
Variant::Variant(const String &value):
    m_type(STRING)
{
    m_dataString = new std::string;
    m_dataString[0] = value;
}

// constructor as real type
Variant::Variant(Real value):
    m_type(REAL),
    m_dataReal(value)
{
}

// constructor as list type
Variant::Variant(const Variant::List &value):
    m_type(LIST)
{
    m_dataList = new List;
    m_dataList[0] = value;
}

// constructor as array type
Variant::Variant(const Variant::Array &value):
    m_type(ARRAY)
{
    m_dataArray = new Array;
    m_dataArray[0] = value;
}

// constructor as dist type
Variant::Variant(const Variant::Dict &value):
    m_type(DICT)
{
    m_dataDict = new Dict;
    m_dataDict[0] = value;
}

// constructor as data type
Variant::Variant(const Variant::Data &value):
    m_type(DATA)
{
    m_dataData = new Data;
    m_dataData[0] = value;
}

// constructor as data type
Variant::Variant(const void *data, size_t size):
    m_type(DATA)
{
    m_dataData = new Data;
    m_dataData->resize(size);
    memcpy(m_dataData->data(), data, size);
}

// initializing constructor as dict type
Variant::Variant(const std::initializer_list<DictPair> &list):
    m_type(DICT)
{
    m_dataDict = new Dict;
    for(auto& obj : list)
        m_dataDict->insert(obj);
}

Variant::Variant(const Variant &obj):
    m_type(UNKNOWN)
{
    setObjectValue(obj);
}

Variant::Variant(Variant &&obj):
    m_type(UNKNOWN)
{
    moveObjectValue(std::move(obj));
}

Variant::~Variant()
{
    freeUpResources();
    m_type = UNKNOWN;
}

// get type of variant
Variant::Type Variant::getType() const
{
    return m_type;
}

// unsafe to get as integer
Variant::Integer Variant::toInteger() const
{
    return m_dataInteger;
}

// unsafe to get as boolean
Variant::Boolean Variant::toBoolean() const
{
    return m_dataBoolean;
}

// unsafe to get as string
const Variant::String &Variant::toString() const
{
    return *m_dataString;
}

// unsafe to get as real
Variant::Real Variant::toReal() const
{
    return m_dataReal;
}

// unsafe to get as list
const Variant::List &Variant::toList() const
{
    return *m_dataList;
}

// unsafe to get as array
const Variant::Array &Variant::toArray() const
{
    return *m_dataArray;
}

// unsafe to get as dict
const Variant::Dict &Variant::toDict() const
{
    return *m_dataDict;
}

// unsafe to get as data
const Variant::Data &Variant::toData() const
{
    return *m_dataData;
}

// safe to cast as integer
Variant::Integer Variant::castToInteger() const
{
    int64_t objInteger;
    switch (m_type) {
    case INTEGER:
        return m_dataInteger;
        break;
    case BOOLEAN:
        return m_dataBoolean?1:0;
        break;
    case REAL:
        return static_cast<int64_t>(m_dataReal);
        break;
    case STRING:
        stringToInteger(*m_dataString, objInteger);
        return objInteger;
        break;
    default:
        break;
    }
    return 0;
}

// safe to cast as boolean
Variant::Boolean Variant::castToBoolean() const
{
    switch (m_type) {
    case BOOLEAN:
        return m_dataBoolean;
        break;
    case INTEGER:
        return m_dataInteger != 0;
        break;
    case REAL:
        return static_cast<int64_t>(m_dataReal) != 0;
        break;
    case STRING:
        return !m_dataString->empty() && m_dataString[0] != "false";
        break;
    case LIST:
        return !m_dataList->empty();
        break;
    case ARRAY:
        return !m_dataArray->empty();
        break;
    case DICT:
        return !m_dataDict->empty();
        break;
    case DATA:
        return !m_dataData->empty();
        break;
    default:
        break;
    }
    return false;
}

// safe to cast as string
Variant::String Variant::castToString() const
{
    std::string objString;
    switch (m_type) {
    case STRING:
        return *m_dataString;
        break;
    case BOOLEAN:
        return m_dataBoolean?"true":"false";
        break;
    case INTEGER:
        integerToString(m_dataInteger, objString);
        break;
    case REAL:
        realToString(m_dataReal, objString);
        break;
    case LIST:
        listToString(*m_dataList, objString);
        break;
    case ARRAY:
        arrayToString(*m_dataArray, objString);
        break;
    case DICT:
        dictToString(*m_dataDict, objString);
        break;
    case DATA:
        dataToString(*m_dataData, objString);
        break;
    default:
        break;
    }
    return objString;
}

// safe to cast as real
Variant::Real Variant::castToReal() const
{
    double objReal;
    switch (m_type) {
    case REAL:
        return m_dataReal;
        break;
    case BOOLEAN:
        return m_dataBoolean?1.0:0.0;
        break;
    case INTEGER:
        return m_dataInteger;
        break;
    case STRING:
        stringToReal(*m_dataString, objReal);
        return objReal;
        break;
    default:
        break;
    }
    return 0.0;
}

// safe to cast as list
Variant::List Variant::castToList() const
{
    List objList;
    switch (m_type) {
    case LIST:
        return *m_dataList;
        break;
    case INTEGER:
        objList.push_back(m_dataInteger);
        break;
    case REAL:
        objList.push_back(m_dataReal);
        break;
    case BOOLEAN:
        objList.push_back(m_dataBoolean);
        break;
    case STRING:
        objList.push_back(*m_dataString);
        break;
    case ARRAY:
        for(auto& obj : *m_dataArray)
            objList.push_back(obj);
        break;
    case DICT:
        for(auto& pair : *m_dataDict)
            objList.push_back(pair.second);
        break;
    case DATA:
        for(auto& byte : *m_dataData)
            objList.push_back(byte);
        break;
    default:
        break;
    }
    return objList;
}

// safe to cast as array
Variant::Array Variant::castToArray() const
{
    Array objArray;
    switch (m_type) {
    case ARRAY:
        return *m_dataArray;
        break;
    case INTEGER:
        objArray.push_back(m_dataInteger);
        break;
    case REAL:
        objArray.push_back(m_dataReal);
        break;
    case BOOLEAN:
        objArray.push_back(m_dataBoolean);
        break;
    case STRING:
        objArray.push_back(*m_dataString);
        break;
    case LIST:
        objArray.reserve(m_dataList->size());
        for(auto& obj : *m_dataList)
            objArray.push_back(obj);
        break;
    case DICT:
        objArray.reserve(m_dataDict->size());
        for(auto& pair : *m_dataDict)
            objArray.push_back(pair.second);
        break;
    case DATA:
        objArray.reserve(m_dataData->size());
        for(auto& byte : *m_dataData)
            objArray.push_back(byte);
        break;
    default:
        break;
    }
    return objArray;
}

// safe to cast as dict
Variant::Dict Variant::castToDict() const
{
    Dict objDict;
    switch (m_type) {
    case DICT:
        return *m_dataDict;
        break;
    default:
        objDict["object"] = *this;
        break;
    }
    return objDict;
}

// safe to cast as data
Variant::Data Variant::castToData() const
{
    Data objData;
    switch (m_type) {
    case DATA:
        return *m_dataData;
        break;
    case INTEGER:
        objData.resize(sizeof(Integer));
        memcpy(objData.data(), &m_dataInteger, sizeof(Integer));
        break;
    case REAL:
        objData.resize(sizeof(Real));
        memcpy(objData.data(), &m_dataReal, sizeof(Real));
        break;
    case BOOLEAN:
        objData.resize(sizeof(Boolean));
        memcpy(objData.data(), &m_dataBoolean, sizeof(Boolean));
        break;
    case STRING:
        objData.resize(m_dataString->size()+1);
        memcpy(objData.data(), m_dataString->c_str(), m_dataString->size()+1);
        break;
    case ARRAY:
        for(auto& obj : *m_dataArray)
        {
            Data data(obj.castToData());
            objData.insert(objData.end(), data.begin(), data.end());
        }
        break;
    case LIST:
        for(auto& obj : *m_dataList)
        {
            Data data(obj.castToData());
            objData.insert(objData.end(), data.begin(), data.end());
        }
        break;
    case DICT:
        for(auto& pair : *m_dataDict)
        {
            Data data(pair.second.castToData());
            objData.insert(objData.end(), data.begin(), data.end());
        }
        break;
    default:
        break;
    }
    return objData;
}

void Variant::setUnknown()
{
    setUnknownValue();
}

// safe to set integer value
void Variant::setInteger(Integer value)
{
    setIntegerValue(value);
}

// safe to set boolean value
void Variant::setBoolean(Boolean value)
{
    setBooleanValue(value);
}

// safe to set string value
void Variant::setString(const String &value)
{
    setStringValue(value);
}

// safe to set real value
void Variant::setReal(Real value)
{
    setRealValue(value);
}

// safe to set list value
void Variant::setList(const Variant::List &value)
{
    setListValue(value);
}

// safe to set array value
void Variant::setArray(const Variant::Array &value)
{
    setArrayValue(value);
}

// safe to set dict value
void Variant::setDict(const Variant::Dict &value)
{
    setDictValue(value);
}

// safe to set data value
void Variant::setData(const Variant::Data &value)
{
    setDataValue(value);
}

// safe to set object value
void Variant::setObject(const Variant &obj)
{
    setObjectValue(obj);
}

// unsafe to get as integer value
Variant::Integer &Variant::getInteger()
{
    return m_dataInteger;
}

// unsafe to get as boolean value
Variant::Boolean &Variant::getBoolean()
{
    return m_dataBoolean;
}

// unsafe to get as string value
Variant::String &Variant::getString()
{
    return m_dataString[0];
}

// unsafe to get as real value
Variant::Real &Variant::getReal()
{
    return m_dataReal;
}

// unsafe to get as list value
Variant::List &Variant::getList()
{
    return m_dataList[0];
}

// unsafe to get as array value
Variant::Array &Variant::getArray()
{
    return m_dataArray[0];
}

// unsafe to get as dict value
Variant::Dict &Variant::getDict()
{
    return m_dataDict[0];
}

// unsafe to get as data value
Variant::Data &Variant::getData()
{
    return m_dataData[0];
}

// safe to set integer value
Variant &Variant::operator=(int value)
{
    setIntegerValue(value);
    return *this;
}

// cast to boolean
Variant::operator bool() const
{
    return castToBoolean();
}

Variant::operator int() const
{
    return castToInteger();
}

Variant::operator double() const
{
    return castToReal();
}

Variant::operator float() const
{
    return castToReal();
}

// safe compare objects
bool Variant::operator==(const Variant &obj) const
{
    if(obj.getType() != m_type)
        return false;
    switch (m_type) {
    case Variant::INTEGER:
        return obj.m_dataInteger == m_dataInteger;
        break;
    case Variant::STRING:
        return obj.m_dataString[0] == m_dataString[0];
        break;
    case Variant::REAL:
        return obj.m_dataReal == m_dataReal;
        break;
    case Variant::BOOLEAN:
        return obj.m_dataBoolean == m_dataBoolean;
        break;
    case Variant::LIST:
        return obj.m_dataList[0] == m_dataList[0];
        break;
    case Variant::ARRAY:
        return obj.m_dataArray[0] == m_dataArray[0];
        break;
    case Variant::DICT:
        return obj.m_dataDict[0] == m_dataDict[0];
        break;
    case Variant::DATA:
        return obj.m_dataData[0] == m_dataData[0];
        break;
    case Variant::UNKNOWN:
        return true;
        break;
    default:
        break;
    }
    return false;
}

// safe to set integer value
Variant &Variant::operator=(Integer value)
{
    setIntegerValue(value);
    return *this;
}

// safe to set boolean value
Variant &Variant::operator=(Boolean value)
{
    setBooleanValue(value);
    return *this;
}

// safe to set string value
Variant &Variant::operator=(const char *value)
{
    setStringValue(value);
    return *this;
}

// safe to set string value
Variant &Variant::operator=(const String &value)
{
    setStringValue(value);
    return *this;
}

// safe to set real value
Variant &Variant::operator=(Real value)
{
    setRealValue(value);
    return *this;
}

// safe to set list value
Variant &Variant::operator=(const Variant::List &value)
{
    setListValue(value);
    return *this;
}

// safe to set array value
Variant &Variant::operator=(const Variant::Array &value)
{
    setArrayValue(value);
    return *this;
}

// safe to set dict value
Variant &Variant::operator=(const Variant::Dict &value)
{
    setDictValue(value);
    return *this;
}

// safe to set data value
Variant &Variant::operator=(const Variant::Data &value)
{
    setDataValue(value);
    return *this;
}

// safe to set object value
Variant &Variant::operator=(const Variant &obj)
{
    setObjectValue(obj);
    return *this;
}

// safe to set object value
Variant &Variant::operator=(Variant &&obj)
{
    moveObjectValue(std::move(obj));
    return *this;
}

// inline convert from string to integer
void Variant::stringToInteger(const String &from, Integer &to) const
{
    std::stringstream ss;
    ss << from;
    ss >> to;
}

// inline convert from integer to string
void Variant::integerToString(Integer from, String &to) const
{
    std::stringstream ss;
    ss << from;
    to = ss.str();
}

// inline convert from string to real
void Variant::stringToReal(const String &from, Real &to) const
{
    std::stringstream ss;
    ss << from;
    ss >> to;
}

// inline convert from real to string
void Variant::realToString(Real from, String &to) const
{
    std::stringstream ss;
    ss << from;
    to = ss.str();
}

// inline convert from list to string
void Variant::listToString(const Variant::List &from, String &to) const
{
    std::stringstream ss;
    bool isFirst = true;
    ss << "(";
    for(auto& obj : from)
    {
        if(!isFirst)
            ss << ",";
        else
            isFirst = false;
        ss << obj.castToString();
    }
    ss << ")";
    to = ss.str();
}

// inline convert from array to string
void Variant::arrayToString(const Variant::Array &from, String &to) const
{
    std::stringstream ss;
    bool isFirst = true;
    ss << "[";
    for(auto& obj : from)
    {
        if(!isFirst)
            ss << ",";
        else
            isFirst = false;
        ss << obj.castToString();
    }
    ss << "]";
    to = ss.str();
}

// inline convert from dict to string
void Variant::dictToString(const Variant::Dict &from, String &to) const
{
    std::stringstream ss;
    bool isFirst = true;
    ss << "{";
    for(auto& pair : from)
    {
        if(!isFirst)
            ss << ";";
        else
            isFirst = false;
        ss << pair.first << ":" << pair.second.castToString();
    }
    ss << "}";
    to = ss.str();
}

// inline convert from data to string
void Variant::dataToString(const Variant::Data &from, String &to) const
{
    const char* characters = "0123456789ABCDEF";
    std::stringstream ss;
    for(auto byte : from)
    {
        ss << characters[byte>>4] << characters[byte&0x0F];
    }
    to = ss.str();
}

// inline safe unknown value
void Variant::setUnknownValue()
{
    freeUpResources();
    m_type = UNKNOWN;
}

// inline safe integer value
void Variant::setIntegerValue(Integer value)
{
    freeUpResources();
    m_type = INTEGER;
    m_dataInteger = value;
}

// inline safe boolean value
void Variant::setBooleanValue(Boolean value)
{
    freeUpResources();
    m_type = BOOLEAN;
    m_dataBoolean = value;
}

// inline safe string value
void Variant::setStringValue(const String &value)
{
    if(m_type != STRING)
    {
        freeUpResources();
        m_dataString = new std::string;
        m_type = STRING;
    }
    m_dataString[0] = value;
}

// inline safe real value
void Variant::setRealValue(Real value)
{
    freeUpResources();
    m_type = REAL;
    m_dataReal = value;
}

// inline safe list value
void Variant::setListValue(const Variant::List &value)
{
    if(m_type != LIST)
    {
        freeUpResources();
        m_dataList = new List;
        m_type = LIST;
    }
    m_dataList[0] = value;
}

// inline safe array value
void Variant::setArrayValue(const Variant::Array &value)
{
    if(m_type != ARRAY)
    {
        freeUpResources();
        m_dataArray = new Array;
        m_type = ARRAY;
    }
    m_dataArray[0] = value;
}

// inline safe dict value
void Variant::setDictValue(const Variant::Dict &value)
{
    if(m_type != DICT)
    {
        freeUpResources();
        m_dataDict = new Dict;
        m_type = DICT;
    }
    m_dataDict[0] = value;
}

// inline safe data value
void Variant::setDataValue(const Variant::Data &value)
{
    if(m_type != DATA)
    {
        freeUpResources();
        m_dataData = new Data;
        m_type = DATA;
    }
    m_dataData[0] = value;
}

// inline safe object value
void Variant::setObjectValue(const Variant &obj)
{
    switch (obj.m_type) {
    case INTEGER:
        setIntegerValue(obj.m_dataInteger);
        break;
    case REAL:
        setRealValue(obj.m_dataReal);
        break;
    case BOOLEAN:
        setBooleanValue(obj.m_dataBoolean);
        break;
    case STRING:
        setStringValue(obj.m_dataString[0]);
        break;
    case LIST:
        setListValue(obj.m_dataList[0]);
        break;
    case ARRAY:
        setArrayValue(obj.m_dataArray[0]);
        break;
    case DICT:
        setDictValue(obj.m_dataDict[0]);
        break;
    case DATA:
        setDataValue(obj.m_dataData[0]);
        break;
    default:
        break;
    }
}

void Variant::moveObjectValue(Variant &&obj)
{
    m_type = obj.m_type;
    switch (m_type) {
    case INTEGER:
        m_dataInteger = obj.m_dataInteger;
        break;
    case BOOLEAN:
        m_dataBoolean = obj.m_dataBoolean;
        break;
    case REAL:
        m_dataReal = obj.m_dataReal;
        break;
    case STRING:
        m_dataString = obj.m_dataString;
        break;
    case LIST:
        m_dataList = obj.m_dataList;
        break;
    case ARRAY:
        m_dataArray = obj.m_dataArray;
        break;
    case DICT:
        m_dataDict = obj.m_dataDict;
        break;
    case DATA:
        m_dataData = obj.m_dataData;
        break;
    default:
        break;
    }
    obj.m_type = UNKNOWN;
}

// free an allocate memory
void Variant::freeUpResources()
{
    switch (m_type) {
    case STRING:
        delete m_dataString;
        break;
    case LIST:
        delete m_dataList;
        break;
    case ARRAY:
        delete m_dataArray;
        break;
    case DICT:
        delete m_dataDict;
        break;
    case DATA:
        delete m_dataData;
        break;
    default:
        break;
    }
}
